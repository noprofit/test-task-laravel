<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Permission\Traits\HasRoles;

class Category extends Model
{
    use NodeTrait, HasRoles, HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public static function withParents(): Collection
    {
        return collect(
            DB::select(
                'select a.name as name, a.id as id, b.name as parent, b.id as parent_id
        from categories a inner join categories b on a.parent_id = b.id'
            )
        );
    }

    public static function parent($id): Collection
    {
        return collect(
            DB::table('categories a')->
            select('a.name as name, a.id as id, b.name pas parent, b.id as parent_id')->join(
                'categories b',
                'a.parent_id',
                '=',
                'b.id',
                "a.id =  $id"
            )
        );
        /*return collect(DB::select(DB::raw("select a.name as name, a.id as id, b.name as parent, b.id as parent_id
        from categories a left join categories b on a.parent_id = b.id where a.id = '$id'")->getValue()))->first();*/
    }
}
