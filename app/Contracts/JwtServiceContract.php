<?php

namespace App\Contracts;

/**
 * Interface JwtServiceProvider
 * Provides aggregate interface to interact with
 * JWT tokens
 */
interface JwtServiceContract
{
    public function decode(string $token): object;

    public function encode(object|array $payload): string;
}
