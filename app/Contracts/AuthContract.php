<?php

namespace App\Contracts;

use Illuminate\Contracts\Auth\Authenticatable;

interface AuthContract
{
    public function authenticate(array $credentials): Authenticatable;
}
