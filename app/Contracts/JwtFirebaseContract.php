<?php

namespace App\Contracts;

interface JwtFirebaseContract extends JwtServiceContract
{
    public function decode(string $token): object;

    public function encode(object|array $payload): string;
}
