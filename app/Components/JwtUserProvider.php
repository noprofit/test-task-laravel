<?php

namespace App\Components;

use App\Contracts\JwtServiceContract;
use App\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Database\Eloquent\Model;

class JwtUserProvider implements UserProvider
{
    public ?Authenticatable $model = null;
    
    public function __construct(
        protected JwtServiceContract $jwt,
        protected Hasher $hasher,
        protected string $modelString
    ) {
    }

    public function retrieveById($identifier): ?Authenticatable
    {
        $model = $this->createModel();
        $this->model = $this->createModel()->newQuery()
            ->where($model->getAuthIdentifierName(), $identifier)
            ->first();

        return $this->model;
    }

    public function retrieveByToken($identifier, $token): ?Authenticatable
    {
        return empty($token) ? null : $this->createModel($this->jwt->decode($token));
    }

    public function updateRememberToken(Authenticatable $user, $token): void
    {
        // TODO: Implement updateRememberToken() method.
    }

    public function retrieveByCredentials(array $credentials): ?Authenticatable
    {
        $model = $this->createModel();
        /** @var Authenticatable $user */
        $user = $model->newQuery()
            ->where($model->getUsernameFieldName(), '=', current($credentials))
            ->first();
        reset($credentials);
        if ($user && $this->validateCredentials($user, $credentials)) {
            $this->model = $user;
        }

        return $this->model;
    }


    public function validateCredentials(Authenticatable $user, array $credentials): bool
    {
        return $this->hasher->check($credentials['password'], $user->getAuthPassword());
    }

    public function createModel($args = []): User
    {
        $class = '\\'.ltrim($this->modelString, '\\');

        return new $class((array)$args);
    }

    public function generateToken(Authenticatable $user): string
    {
        return $this->jwt->encode([
            $user->getUsernameFieldName() => $user->{$user->getUsernameFieldName()},
            $user->getPasswordFieldName() => $user->{$user->getPasswordFieldName()}
            ]);
    }
}
