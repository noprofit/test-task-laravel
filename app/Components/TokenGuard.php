<?php

namespace App\Components;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;

class TokenGuard implements Guard
{
    protected ?Authenticatable $user;
    protected UserProvider $provider;
    protected Request $request;

    public function __construct(UserProvider $provider, $request, Authenticatable $user = null)
    {
        $this->provider = $provider;
        $this->request = $request;
        $this->user = $user;
    }

    public function check(): bool
    {
        return ! is_null($this->user());
    }

    public function guest(): bool
    {
        return ! $this->check();
    }



    public function user(): ?Authenticatable
    {
        if (! is_null($this->user)) {
            return $this->user;
        }

        $token = $this->request->bearerToken();
        /** @var Authenticatable $user */
        $user = $this->provider->retrieveByToken('id', $token);

        $this->setUser($user);

        return $this->user;
    }

    public function id(): mixed
    {
        if ($this->user) {
            return $this->user->getAuthIdentifier();
        }

        return null;
    }

    public function validate(array $credentials = []): bool
    {
        if (!empty($credentials)) {
            $password = $credentials['password'];
        } else {
            if (!is_null($this->user)) {
                $password = $this->user->getAuthPassword();
            } else {
                return false;
            }
        }

        if ($this->provider->validateCredentials($this->user, ['password' => $password])) {
            return true;
        }

        return false;
    }

    public function setUser(?Authenticatable $user): void
    {
        $this->user = $user;
    }

    public function setRequest($request): void
    {
        $this->request = $request;
    }

    public function attempt(): string
    {
        return $this->provider->generateToken($this->user);
    }

    public function hasUser(): bool
    {
        return is_null($this->user);
    }
}
