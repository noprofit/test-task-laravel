<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\Filesystem;
use LogicException;
use RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class GenerateKeyPairCommand
 * Took the idea/configuration (tbh, all of the code too, too beautiful..)
 * from Lexik JWT package
 */
final class GenerateKeyPairCommand extends Command
{
    private const ACCEPTED_ALGORITHMS = [
        'RS256',
        'RS384',
        'RS512',
        'HS256',
        'HS384',
        'HS512',
        'ES256',
        'ES384',
        'ES512',
    ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jwt:generate-keypair';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates JWT private/public key pair';

    /**
     * Create a new command instance.
     *
     * @param Filesystem $filesystem
     * @param string|null $private
     * @param string|null $public
     * @param string|null $pass
     * @param string|null $algorithm
     */
    public function __construct(
        private Filesystem $filesystem,
        private ?string $private,
        private ?string $public,
        private ?string $pass,
        private ?string $algorithm
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Generate public/private keys for use in your application.');
        $this->addOption('dry-run', null, InputOption::VALUE_NONE, 'Do not update key files.');
        $this->addOption('skip-if-exists', null, InputOption::VALUE_NONE, 'Do not update key files if they already exist.');
        $this->addOption('overwrite', null, InputOption::VALUE_NONE, 'Overwrite key files if they already exist.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        return $this->handle($input, $output);
    }
    /**
     * Execute the console command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function handle(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if (!in_array($this->algorithm, self::ACCEPTED_ALGORITHMS, true)) {
            $io->error(sprintf('Cannot generate key pair with the provided algorithm `%s`.', $this->algorithm));

            return 1;
        }

        [$private, $public] = $this->generateKeyPair($this->pass);

        if (true === $input->getOption('dry-run')) {
            $io->success('Your keys have been generated!');
            $io->newLine();
            $io->writeln(sprintf('Update your private key in <info>%s</info>:', $this->private));
            $io->writeln($private);
            $io->newLine();
            $io->writeln(sprintf('Update your public key in <info>%s</info>:', $this->public));
            $io->writeln($public);

            return 0;
        }

        if (!$this->private || !$this->public) {
            throw new LogicException(sprintf('The "private_key" and "public_key" config options must not be empty for using the "%s" command.', self::$defaultName));
        }

        $alreadyExists = $this->filesystem->exists($this->private) || $this->filesystem->exists($this->public);

        if ($alreadyExists) {
            try {
                $this->handleExistingKeys($input);
            } catch (RuntimeException $e) {
                if (0 === $e->getCode()) {
                    $io->comment($e->getMessage());

                    return 0;
                }

                $io->error($e->getMessage());

                return 1;
            }

            if (!$io->confirm('You are about to replace your existing keys. Are you sure you wish to continue?')) {
                $io->comment('Your action was canceled.');

                return 0;
            }
        }

        $this->filesystem->put($this->private, $private);
        $this->filesystem->put($this->public, $public);

        $io->success('Done!');

        return 0;
    }

    private function buildOpenSSLConfiguration(): array
    {
        $digestAlgorithms = [
            'RS256' => 'sha256',
            'RS384' => 'sha384',
            'RS512' => 'sha512',
            'HS256' => 'sha256',
            'HS384' => 'sha384',
            'HS512' => 'sha512',
            'ES256' => 'sha256',
            'ES384' => 'sha384',
            'ES512' => 'sha512',
        ];
        $privateKeyBits = [
            'RS256' => 2048,
            'RS384' => 2048,
            'RS512' => 4096,
            'HS256' => 384,
            'HS384' => 384,
            'HS512' => 512,
            'ES256' => 384,
            'ES384' => 512,
            'ES512' => 1024,
        ];
        $privateKeyTypes = [
            'RS256' => \OPENSSL_KEYTYPE_RSA,
            'RS384' => \OPENSSL_KEYTYPE_RSA,
            'RS512' => \OPENSSL_KEYTYPE_RSA,
            'HS256' => \OPENSSL_KEYTYPE_DH,
            'HS384' => \OPENSSL_KEYTYPE_DH,
            'HS512' => \OPENSSL_KEYTYPE_DH,
            'ES256' => \OPENSSL_KEYTYPE_EC,
            'ES384' => \OPENSSL_KEYTYPE_EC,
            'ES512' => \OPENSSL_KEYTYPE_EC,
        ];

        $curves = [
            'ES256' => 'secp256k1',
            'ES384' => 'secp384r1',
            'ES512' => 'secp521r1',
        ];

        $config = [
            'digest_alg' => $digestAlgorithms[$this->algorithm],
            'private_key_type' => $privateKeyTypes[$this->algorithm],
            'private_key_bits' => $privateKeyBits[$this->algorithm],
        ];

        if (isset($curves[$this->algorithm])) {
            $config['curve_name'] = $curves[$this->algorithm];
        }

        return $config;
    }

    private function handleExistingKeys(InputInterface $input): void
    {
        if (true === $input->getOption('skip-if-exists') && true === $input->getOption('overwrite')) {
            throw new RuntimeException('Both options `--skip-if-exists` and `--overwrite` cannot be combined.', 1);
        }

        if (true === $input->getOption('skip-if-exists')) {
            throw new RuntimeException('Your key files already exist, they won\'t be overriden.', 0);
        }

        if (false === $input->getOption('overwrite')) {
            throw new RuntimeException('Your keys already exist. Use the `--overwrite` option to force regeneration.', 1);
        }
    }

    private function generateKeyPair($passphrase): array
    {
        $config = $this->buildOpenSSLConfiguration();

        $resource = \openssl_pkey_new($config);
        if (false === $resource) {
            throw new RuntimeException(\openssl_error_string());
        }

        $success = \openssl_pkey_export($resource, $privateKey, $passphrase);

        if (false === $success) {
            throw new RuntimeException(\openssl_error_string());
        }

        $publicData = \openssl_pkey_get_details($resource);

        if (false === $publicData) {
            throw new RuntimeException(\openssl_error_string());
        }

        $public = $publicData['key'];

        return [$privateKey, $public];
    }
}
