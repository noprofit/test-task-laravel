<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response as BaseResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Throwable $e
     * @return BaseResponse|Response
     * @throws Throwable
     */

    public function render($request, Throwable $e)
    {
        if (\config('app.debug')) {
            $response = new Response(get_class($e) . "\n" . $e->getTraceAsString(), 400);
        } else {
            Log::error(get_class($e) . "\n" . $e->getTraceAsString());
            if ($e instanceof HttpException) {
                $response = new JsonResponse([
                    'status' => 'error',
                    'meessage' => $e->getMessage()
                ], 400);
            } else {
                $response = new JsonResponse('something went wrong', 400);
            }
        }
        Log::error($e->getMessage() . "\n" . $e->getTraceAsString());
        return $response;
    }
}
