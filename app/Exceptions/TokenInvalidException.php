<?php

namespace App\Exceptions;

use Exception;

class TokenInvalidException extends Exception
{
    /**
     * TokenInvalidException constructor.
     * @param $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
