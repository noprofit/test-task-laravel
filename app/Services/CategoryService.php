<?php

namespace App\Services;

use App\Models\Category;
use Illuminate\Support\Facades\DB;

class CategoryService
{
    public function create(&$request): Category
    {
        ['name' => $name, 'parent_id' => $parent_id] = $request->validated();
        $parent = Category::findOrFail($parent_id);
        return Category::create($request->all(), $parent);
    }

    public function update(&$request): Category
    {
        $id = $request->segment(count($request->segments()));
        if (is_numeric($id)) { // patch
            $category = Category::findOrFail($id);
            ['name' => $name, 'parent_id' => $parent_id] = $request->validated();
            if ($parent_id !== null) {
                $category->appendOrPrependTo(
                    Category::findOrfail($parent_id)
                );
            }
            if ($name !== null) {
                $category->name = $name;
            }
            DB::beginTransaction();
            $category->save();
            DB::commit();
            return $category;
        }

        return $this->create($request);
    }

    public function delete(&$request): void
    {
        $id = $request->segment(count($request->segments()));
        if (is_numeric($id)) { // delete
            DB::beginTransaction();
            Category::findOrFail($id)->delete();
            DB::commit();
        }
    }
}
