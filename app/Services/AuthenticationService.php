<?php

namespace App\Services;

use App\Components\JwtUserProvider;
use App\Contracts\AuthContract;
use App\Contracts\JwtServiceContract;
use App\User;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticationService implements AuthContract
{
    public function __construct(
        protected JwtServiceContract $jwtService,
        protected JwtUserProvider $jwtUserProvider
    ) {
    }

    public function authenticate(array $credentials): Authenticatable
    {
        $user = $this->jwtUserProvider->retrieveByCredentials($credentials);
        if (is_null($user)) {
            throw new AuthenticationException('Error, user not found');
        }

        Auth::guard()->setUser($user);

        return $user;
    }

    public function createToken(Authenticatable $user): string
    {
        $token =  Auth::guard()->attempt();
        $user->refresh_token = $this->jwtService->encode([$token]);
        $user->save();

        return $token;
    }

    public function refreshToken(Request $request): string
    {
        $identity = User::where('refresh_token', $request->bearerToken())->first();
        if (is_null($identity)) {
            throw new AuthenticationException('Error, user not found');
        }

        $guard = Auth::guard();
        $guard->setUser($identity);

        return $this->createToken($guard->user());
    }
}
