<?php

namespace App\Services;

use App\Contracts\JwtFirebaseContract;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Support\Facades\Hash;

class JwtFirebaseServiceWrapper implements JwtFirebaseContract
{
    public function __construct(
        protected JWT $jwt,
        protected Key $private,
        protected string $passphrase
    ) {
    }

    public function decode(string $token): object
    {
        $privateKey = openssl_pkey_get_private(
            'file://'.$this->private->getKeyMaterial(),
            $this->passphrase
        );
        $publicKey = openssl_pkey_get_details($privateKey)['key'];

        return $this->jwt::class::decode($token, $publicKey, ['RS256']);
    }

    public function encode(object|array $payload): string
    {
        $privateKey = openssl_pkey_get_private(
            'file://'.$this->private->getKeyMaterial(),
            $this->passphrase
        );

        return $this->jwt::class::encode(
            $payload,
            $privateKey,
            'RS256',
            null,
            [
                'jti' => Hash::make((string)time()),
                'exp' => 3600,
                'iat' => time(),
            ]
        );
    }
}
