<?php

namespace App\Policies;

use App\Models\Category;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryPolicy
{
    use HandlesAuthorization;
    /**
     * @param Category
     */
    protected $category;
    public function __construct(Request $request)
    {
        $this->category = $request->route('category');
    }

    public function update(User $user)
    {
        return $user->isAdmin();
    }

    public function create(User $user)
    {
        return $this->update($user);
    }

    public function delete(User $user)
    {
        return $this->update($user);
    }

    // unused, but potentially passable
    public function view(User $user)
    {
        return Auth::check();
    }

    public function viewAny(User $user)
    {
        return $this->view($user);
    }
}
