<?php

namespace App\Providers;

use App\Components\JwtUserProvider;
use App\Components\TokenGuard;
use App\Contracts\JwtServiceContract;
use App\Models\Category;
use App\Policies\CategoryPolicy;
use App\Services\JwtFirebaseServiceWrapper;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Hashing\HashManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Category::class => CategoryPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->registerPolicies();

        $this->app->singleton('user-provider', function () {
            return new EloquentUserProvider(
                \app(HashManager::class),
                \config('auth.providers.users.model')
            );
        });

        $this->app->singleton(JwtServiceContract::class, function ($app) {
            return new JwtFirebaseServiceWrapper(
                new JWT(),
                new Key(
                    \storage_path('keys').'/'.\config('jwt.private_key'),
                    \config('jwt.algorithm')
                ),
                \config('jwt.passphrase')
            );
        });

        $this->app->singleton(JwtUserProvider::class, function ($app, array $config) {
            return new  JwtUserProvider(
                $app->get(JwtServiceContract::class),
                \app(HashManager::class),
                \config('auth.providers.users.model')
            );
        });

        Auth::extend('token-guard', function ($app, $name, array $config) {
            return new TokenGuard(
                Auth::createUserProvider($config['provider']),
                \app('request')
            );
        });
    }

    public function register(): void
    {
        Auth::provider('jwt', function ($app, array $config) {
            return $app->get(JwtUserProvider::class);
        });
    }
}
