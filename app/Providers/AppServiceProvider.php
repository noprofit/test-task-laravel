<?php

namespace App\Providers;

use App\Console\Commands\GenerateKeyPairCommand;
use App\Services\CategoryService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(CategoryService::class, function () {
            return new CategoryService();
        });

        $this->app->singleton(GenerateKeyPairCommand::class, function ($app) {
            return new GenerateKeyPairCommand(
                $app->get('filesystem')->disk('jwt'),
                \config('jwt.private_key'),
                \config('jwt.public_key'),
                \config('jwt.passphrase'),
                \config('jwt.algorithm'),
            );
        });
    }
}
