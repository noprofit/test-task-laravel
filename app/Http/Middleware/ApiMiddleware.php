<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class ApiMiddleware
{
    public function handle($request, Closure $next)
    {
        if (is_null($request->bearerToken())) {
            return new JsonResponse(['error' => 'Token is missing.'], 400);
        }

        if (is_null(Auth::guard('token')->user())) {
            return new JsonResponse(['error' => 'Token validation failed.'], 400);
        }
        return $next($request);
    }
}
