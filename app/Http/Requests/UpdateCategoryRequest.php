<?php

namespace App\Http\Requests;

final class UpdateCategoryRequest extends BaseRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'string|max:255',
            'parent_id' => 'integer'
        ];
    }

    public function messages(): array
    {
        return [
            'parent_id.integer' => 'Wrong id type',
            'name.string' => 'Name type mismatch',
            'name.max' => 'Name is should be 255 characters long'
            ];
    }
}
