<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class AuthRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'email' => 'required|string|max:64',
            'password' => 'required|string|max:64'
            ];
    }

    /**
     * @inheritdoc
     */
    public function messages(): array
    {
        return [
            'email.required' => 'Email is required',
            'email.integer' => 'Illegal character',
            'email.max' => 'Email should not be longer than 64 characters',
            'password.required' => 'Password is mandatory',
            'password.string' => 'Illegal character',
            'password.max' => 'Password should not be longer than 64 characters'
        ];
    }

    /**
     * @param null $key
     * @param null $default
     * @return array
     * @throws ValidationException
     */
    public function validated($key = null, $default = null): array
    {
        if ($this->accepts('*/*') || $this->expectsJson()) {
            return $this->validator->validate();
        }

        throw new ValidationException(
            $this->validator,
            new JsonResponse([
                'status' => 'error',
                'message' => 'wrong json, or content type'
            ])
        );
    }
}
