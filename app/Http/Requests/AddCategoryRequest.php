<?php

namespace App\Http\Requests;

final class AddCategoryRequest extends BaseRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'parent_id' => 'required|integer'
        ];
    }

    public function messages(): array
    {
        return [
            'parent_id.required' => 'An id of a parent is required for this operation',
            'parent_id.integer' => 'Wrong id type',
            'name.required' => 'Name parameter is mandatory',
            'name.string' => 'Name type mismatch',
            'name.max' => 'Name is should be 255 characters long'
        ];
    }
}
