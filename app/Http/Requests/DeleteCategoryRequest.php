<?php

namespace App\Http\Requests;

class DeleteCategoryRequest extends BaseRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    public function messages(): array
    {
        return [

        ];
    }
}
