<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Responses\SuccessResponse;
use App\Http\Requests\AddCategoryRequest;
use App\Http\Requests\DeleteCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Http\Resources\CategoriesResource;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryController extends Controller
{
    public function __construct(protected CategoryService $category_service)
    {
        $this->authorizeResource(Category::class, Category::class);
    }

    protected function resourceAbilityMap(): array
    {
        return array_merge(
            parent::resourceAbilityMap(),
            [
                'add' => 'create',
                'delete' => 'delete',
                'update' => 'update',
                'get' => 'view',
            ]
        );
    }


    public function add(AddCategoryRequest $request): JsonResponse
    {
        $category = $this->category_service->create($request);

        return new JsonResponse([
            'status' => 'success',
            'message' => 'Category created.',
            'data' => new CategoryResource($category)]);
    }

    public function update(UpdateCategoryRequest $request): JsonResource
    {
        $category = $this->category_service->update($request);

        return new JsonResource([
            'status' => 'success',
            'message' => 'Category updated.',
            'data' => new CategoryResource($category)
        ]);
    }


    public function delete(DeleteCategoryRequest $request): JsonResponse
    {
        $this->category_service->delete($request);

        return SuccessResponse::make('Delete successful');
    }

    public function get($category = null): CategoryResource|CategoriesResource|JsonResource
    {
        if (!is_null($category)) {
            return new CategoryResource(Category::parent($category->id));
        }

        return new CategoriesResource(Category::withParents());
    }
}
