<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\AuthRequest;
use App\Http\Requests\BasicAuthRequest;
use App\Services\AuthenticationService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AuthController
{
    public const TOKEN_EXPIRES = 3600;

    public function __construct(
        protected AuthenticationService $authenticationService
    ) {
    }

    public function authenticate(AuthRequest $request): JsonResponse
    {
        try {
            $credentials = $request->validated();
            $user = $this->authenticationService->authenticate($credentials);
            $token = $this->authenticationService->createToken($user);
        } catch (Exception $e) {
            Log::error('Authentication error:', [
                'exception' => $e->getMessage(),
                'code' => $e->getCode(),
                'trace' => $e->getTraceAsString()
                ]);
            return new JsonResponse([
                'status' => 'error',
                'message' => 'authentication error'
            ]);
        }

        return new JsonResponse([
            'access_token' => $token,
            'refresh_token' => $user->refresh_token,
            'token_type' => 'Bearer',
            'expires_in' => static::TOKEN_EXPIRES,
        ]);
    }

    public function refresh(BasicAuthRequest $request): JsonResponse
    {
        try {
            $token = $this->authenticationService->refreshToken($request);
        } catch(Exception $e) {
            return new JsonResponse([
                'status' => 'error',
                'message' => 'authentication error'
            ]);
        }

        return new JsonResponse([
            'access_token' => $token,
            'refresh_token' => Auth::user()->refresh_token,
            'token_type' => 'Bearer',
            'expires_in' => static::TOKEN_EXPIRES,
        ]);
    }
}
