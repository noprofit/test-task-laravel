<?php

namespace App\Http\Controllers\Responses;

use Illuminate\Http\JsonResponse;

class SuccessResponse extends JsonResponse
{
    /**
     * @param string $msg
     * @return JsonResponse
     */
    public static function make($msg = ''): JsonResponse
    {
        $data = ['status' => 'success'];
        $data['message'] = $msg;

        return new static($data);
    }
}
