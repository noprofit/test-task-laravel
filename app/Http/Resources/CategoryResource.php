<?php

namespace App\Http\Resources;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof Category) {
            return [
                'id' => $this->resource->id,
                'name' => $this->resource->name,
                'parent_id' => $this->resource->parent_id
                ];
        }

        return (array)$this->resource;
    }
}
