<?php

namespace Tests\Unit\Api;

use App\Models\Category;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ApiCategoryTest extends TestCase
{
    use DatabaseTransactions;
    protected array $token;
    protected Generator $faker;
    protected int $parent_id;

    public function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
        $this->parent_id = Category::all()->first()->id;
    }

    protected function initTokenFor($user, $password): void
    {
        $response = $this->post(
            \route('jwt-auth'),
            [
                'email' => $user,
                'password' => $password
            ]
        );

        $this->token = $response->getData(true);
    }

    /**
     * @dataProvider provideAuthData
     * @param string $username
     * @param string $password
     */
    public function testGetCategory(string $username, string $password): void
    {
        $this->initTokenFor($username, $password);
        $response = $this->get(
            \route('api-get-category', ['category' => 1]),
            ['authorization' => $this->token['token_type'] . ' ' . $this->token['access_token']]
        );

        $response->assertStatus(200);
    }

    /**
     * @dataProvider provideAuthData
     * @param string $username
     * @param string $password
     */
    public function testGetCategories(string $username, string $password): void
    {
        $this->initTokenFor($username, $password);
        $response = $this->get(
            \route('api-get-categories'),
            ['authorization' => $this->token['token_type'] . ' ' . $this->token['access_token']]
        );

        $response->assertStatus(200);
    }

    /**
     * @dataProvider provideCategoryTestData
     * @param string $username
     * @param string $password
     * @param array $categoryData
     * @param bool $expectedResult
     */
    public function testUserCreateCategory(
        string $username,
        string $password,
        object $categoryData,
        bool $expectedResult
    ): void
    {
        $this->initTokenFor($username, $password);
        $response = $this->post(
            \route('api-create-category'),
            [
                'name' => $categoryData->name,
                'parent_id' => $categoryData->parent_id,
            ],
            ['authorization' => $this->token['token_type'] . ' ' . $this->token['access_token']]
        );
        if ($expectedResult) {
            $response->assertSee('data');
            $this->assertInstanceOf(Category::class, Category::find(1));
        } else {
            $response->assertStatus(400);
        }
    }

    public static function provideAuthData(): array
    {
        return [
            [
                'username' => 'admin@example.com',
                'password' => 'admin'
            ],
            [
                'username' => 'test@example.com',
                'password' => 'test'
            ],
        ];
    }

    public static function provideCategoryTestData(): array
    {
        return [
            [
                'username' => 'admin@example.com',
                'password' => 'admin',
                'category data' => (object)['name' => 'anton', 'parent_id' => 1],
                'result' => true
            ],
            [
                'username' => 'test@example.com',
                'password' => 'test',
                'category data' => (object)['name' => 'anton', 'parent_id' => 1],
                'result' => false
            ],
        ];
    }
}
