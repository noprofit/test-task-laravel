<?php

namespace Tests\Unit\Auth;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthorizationTest extends TestCase
{
    protected array $token;

    public function setUp(): void
    {
        parent::setUp();
    }

    protected function initTokenFor($user, $password): void
    {
        $response = $this->post(
            \route('jwt-auth'),
            [
                'email' => $user,
                'password' => $password
            ]
        );

        $this->token = $response->getData(true);
    }

    public function testGuestNoAuth(): void
    {
        $response = $this->get(\route('api-get-categories'));
        $response->assertStatus(400);
        $response->assertSee('error');
    }

    /**
     * @dataProvider provideAuthData
     * @param string $username
     * @param string $password
     * @param bool $expected
     */
    public function testUsersAuthentication(string $username, string $password, bool $expected): void
    {
        $this->initTokenFor($username, $password);
        if ($expected) {
            $this->assertIsString($this->token['access_token']);
        } else {
            $this->assertEquals('error', $this->token['status']);
        }
    }

    public static function provideAuthData(): array
    {
        return [
            [
                'username' => 'admin@example.com',
                'password' => 'admin',
                'expected' => true,
            ],
            [
                'username' => 'test@example.com',
                'password' => 'test',
                'expected' => true,
            ],
            [
                'username' => 'sjdkfkfuIDklusdfjhkhfsd@123234dkdsfjhsdf.com',
                'password' => 'sdfsdfa',
                'expected' => false,
            ],
        ];
    }
}
