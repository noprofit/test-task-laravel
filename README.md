# Test task
## Requirements
- php83
- sqlite3,
- pdo_sqlite
### copy env
```
cp .env.example .env
```
### replace TOKEN_SECRET 
```
sed -i "s/TOKEN_SECRET.*$/TOKEN_SECRET\=$(echo $(dd status=none if=/dev/random bs=3 count=3) | md5sum | awk '{print $1}')/g" .env
```
### create sqlite datafile (or uncomment mysql part in config) 
### edit .env DATABASE path if needed, run migrations, generate jwt key pair
```
touch storage/data.db
// replace if needed DATABASE part in .env
// do migrations
php artisan migrate --seed
// if want to use keypair
php artisan jwt:generate-keypair
// tests
php phpunit
// serve
php artisan serve
```
