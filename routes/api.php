<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;
// api
Route::prefix('v1')->group(function () {
    // auth
    Route::middleware(['jwt'])->group(function() {
        Route::post('auth', [AuthController::class, 'authenticate'])->name('jwt-auth');
        Route::post('refresh', [AuthController::class, 'refresh'])->name('jwt-refresh');
    });
    // categories
    Route::middleware(['api'])->group(function () {
        Route::prefix('category')->group(function () {
            Route::get('/', [CategoryController::class, 'get'])->name('api-get-categories');
            Route::get('/{category}', [CategoryController::class, 'get'])->name('api-get-category');
            Route::post('', [CategoryController::class, 'add'])->name('api-create-category');
            Route::put('/{id?}', [CategoryController::class, 'update'])->name('api-update-category');
            Route::patch('/{id}', [CategoryController::class, 'update'])->name('api-patch-category');
            Route::delete('/{category}', [CategoryController::class, 'delete'])->name('api-delete-category');
        });
    });
});
