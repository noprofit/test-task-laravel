<?php
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
})->name('home');
Route::get('/api', function() {
    return new JsonResponse(['status' => 'success', 'message' => 'you\'re on']);
})->name('api');
