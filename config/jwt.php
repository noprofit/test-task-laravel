<?php
return [
    'private_key' => env('JWT_PRIVATE_KEY', 'jwt25519.pem'),
    'public_key' => env('JWT_PUBLIC_KEY', 'pub25519.pem'),
    'passphrase' => env('JWT_PASSPHRASE', 'change_it_really'),
    'algorithm' => env('SIGNATURE_ALGORITHM', 'RS256'),
];
