<?php

use App\Models\Category;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // admin / admin
        $admin = User::factory()->make([
            'name' => 'admin',
            'email' => 'admin@example.com',
            'password' => Hash::make('admin'),
        ]);
        $admin->save();
        // test / test
        DB::table('users')->insert([
            'name' => 'test',
            'email' => 'test@example.com',
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'password' => Hash::make('test'),
        ]);

        // parent cat
        //$parent_to_be_id = factory(Category::class, 1)->create()->first()->id;
        $parentCategory = Category::factory()->make(['id' => 1]);
        $parentCategory->save();
        // three subs
        Category::factory()->count(3)->make([
            'parent_id' => $parentCategory->id,
        ])->each(function (Category $category) {
            $category->save();
        });
        /*
        factory(Category::class, 3)->create()->each(function ($category) use ($parent_to_be_id) {
            $category->parent_id = $parent_to_be_id;
            $category->save();
        });*/
    }
}
