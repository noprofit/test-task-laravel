<?php

declare(strict_types=1);

include __DIR__ . '/vendor/autoload.php';

$finder = (new PhpCsFixer\Finder())
    ->in('app')
    ->ignoreDotFiles(true)
;

return (new PhpCsFixer\Config())
    ->setRules([
        '@PSR12' => true,
        'no_alternative_syntax' => true,
        'strict_comparison' => true,
        'strict_param' => false,
        'declare_strict_types' => false,
        'yoda_style' => false,
        'phpdoc_to_return_type' => true,
        'void_return' => true,
        'fully_qualified_strict_types' => false
    ])
    ->setFinder($finder)
    ->setUsingCache(false)
    ->setRiskyAllowed(true)
    ->setUsingCache(false)
;
